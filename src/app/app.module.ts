import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { HashLocationStrategy, LocationStrategy } from '@angular/common'
import { AppComponent } from './app.component';
import { LandingComponent } from './landing/landing.component';
import { OpenPositionsComponent } from './open-positions/open-positions.component';
import { CeoMessageComponent } from './ceo-message/ceo-message.component';

@NgModule({
  declarations: [
    AppComponent,
    LandingComponent,
    OpenPositionsComponent,
    CeoMessageComponent,
  ],
  imports: [
    BrowserModule,

    RouterModule.forRoot([
      { path: 'landing', component: LandingComponent },
      { path: 'open-positions', component: OpenPositionsComponent },
      { path: 'ceo-message', component: CeoMessageComponent },
      { path: '**', redirectTo: 'landing' },
    ])
  ],
  providers: [{provide: LocationStrategy, useClass: HashLocationStrategy}],
  bootstrap: [AppComponent]
})
export class AppModule { }
