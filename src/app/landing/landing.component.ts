import { Component, OnInit, Input, ViewChild, ElementRef } from '@angular/core';

@Component({
  selector: 'app-landing',
  templateUrl: './landing.component.html',
  styleUrls: ['./landing.component.scss']
})
export class LandingComponent implements OnInit {

  @ViewChild('roll')
  roll: ElementRef;

  members = [
    {
      url: './assets/images/melody.jpg',
      hover: './assets/images/melody-hover.jpg',
      name: 'Mel',
      fact: 'I am a huge foodie and love finding hole-in-the-wall restaurants.',
    },
    // {
    //   url: './assets/images/team-carousel.png',
    //   name: 'Abhishek',
    // },
    // {
    //   url: './assets/images/team-carousel.png',
    //   name: 'Aditya',
    // },
    // {
    //   url: './assets/images/amie.jpg',
    //   hover: './assets/images/amie-hover.jpg',
    //   name: 'Amie',
    // },
    {
      url: './assets/images/bhanu.jpg',
      hover: './assets/images/bhanu-hover.jpg',
      name: 'Bhanu',
      fact: 'I chased a black bear in a forest in India on foot to take a picture. That bear led us to a sleeping leopard.',
    },
    {
      url: './assets/images/cam.jpg',
      hover: './assets/images/cam-hover.jpg',
      name: 'Cam',
      fact: 'I will play "Free Bird" upon request.'
    },
    {
      url: './assets/images/charles.jpg',
      hover: './assets/images/charles-hover.jpg',
      name: 'Charles',
      fact: 'Begone!'
    },
    {
      url: './assets/images/chuck.jpg',
      hover: './assets/images/chuck-hover.jpg',
      name: 'Chuck',
      fact: 'I have swum with sharks!'
    },
    {
      url: './assets/images/dani.jpg',
      hover: './assets/images/dani-hover.jpg',
      name: 'Dani',
      fact: 'My favorite Avenger is Thor.'
    },
    {
      url: './assets/images/giri.jpg',
      hover: './assets/images/giri-hover.jpg',
      name: 'Giri',
      fact: 'Creator of "Awkward Indian Salsa".',
    },
    {
      url: './assets/images/jamaal.jpg',
      hover: './assets/images/jamaal-hover.jpg',
      name: 'Jamaal',
      fact: 'I can deadlift 425lbs.',
    },
    {
      url: './assets/images/javier.jpg',
      hover: './assets/images/javier-hover.jpg',
      name: 'Javier',
      fact: 'You\'re hired!',
    },
    {
      url: './assets/images/amanda.jpg',
      hover: './assets/images/amanda-hover.jpg',
      name: 'Amanda',
      fact: 'I have freckles on my lips!',
    },
    {
      url: './assets/images/jaya.jpg',
      hover: './assets/images/jaya-hover.jpg',
      name: 'Jayashree',
      fact: 'I love bugs, I breathe bugs, I laugh bugs. I eat bugs. Oops no, I am a vegetarian!'
    },
    {
      url: './assets/images/jony.jpg',
      hover: './assets/images/jony-hover.jpg',
      name: 'Jony',
      fact: 'Half of my problems are because of the tone of my voice.'
    },
    // {
    //   url: './assets/images/karthik.jpg',
    //   hover: './assets/images/karthik-hover.jpg',
    //   name: 'Karthik',
    // },
    {
      url: './assets/images/kevin.jpg',
      hover: './assets/images/kevin-hover.jpg',
      name: 'Kevin',
      fact: 'Venezuelan arepas are my ultimate comfort food!'
    },
    {
      url: './assets/images/manohara.jpg',
      hover: './assets/images/manohara-hover.jpg',
      name: 'Manohara',
      fact: 'The first time I rode a bicycle was across Goa through the city, a rainforest, a mining quarry, a wildlife sanctuary and a four-tiered waterfall covering 250 Km in 5 days.'
    },
    {
      url: './assets/images/mike.jpg',
      hover: './assets/images/mike-hover.jpg',
      name: 'Mike',
      fact: 'I can deadlift Jamaal.'
    },
    {
      url: './assets/images/moh.jpg',
      hover: './assets/images/moh-hover.jpg',
      name: 'Moh',
      fact: 'Part of my morning routine is to pass by a road fish on my way to work.'
    },
    // {
    //   url: './assets/images/team-carousel.jpg',
    //   name: 'Parul',
    // },
    // {
    //   url: './assets/images/pavan.jpg',
    //   hover: './assets/images/pavan-hover.jpg',
    //   name: 'Pavan',
    // },
    {
      url: './assets/images/priyal.jpg',
      hover: './assets/images/priyal-hover.jpg',
      name: 'Priyal',
      fact: '74 of the 90 books I own have been rescued from recycling.'
    },
    {
      url: './assets/images/ron.jpg',
      hover: './assets/images/ron-hover.jpg',
      name: 'Ron',
      fact: '8 letters 2 words and you\'ve got my attention...GOOD FOOD.'
    },
    {
      url: './assets/images/rubin.jpg',
      hover: './assets/images/rubin-hover.jpg',
      name: 'Rubin',
      fact: 'I was a lakers fan before Lebron.'
    },
    {
      url: './assets/images/ryan.jpg',
      hover: './assets/images/ryan-hover.jpg',
      name: 'Ryan',
      fact: 'There are no fun facts.'
    },
    {
      url: './assets/images/salman.jpg',
      hover: './assets/images/salman-hover.jpg',
      name: 'Salman',
      fact: 'I have been to 62 concerts since 2009, but never in February.',
    },
    {
      url: './assets/images/sufi.jpg',
      hover: './assets/images/sufi-hover.jpg',
      name: 'Sufi',
      fact: 'I love woodworking and have built a few pieces of furniture for my home that are still standing.'
    },
    {
      url: './assets/images/timr.jpg',
      hover: './assets/images/timr-hover.jpg',
      name: 'Tim R.',
      fact: 'I am wanted by both the Empire and the Rebels.'
    },
    {
      url: './assets/images/tims.jpg',
      hover: './assets/images/tims-hover.jpg',
      name: 'Tim S.',
      fact: 'When I was about 7 or 8, I participated in a banana eating competition.  I lost, I lost bad.  To this day I still cannot eat a banana.'
    },
    {
      url: './assets/images/wilbin.jpg',
      hover: './assets/images/wilbin-hover.jpg',
      name: 'Wilbin',
      fact: ''
    },
    {
      url: './assets/images/yousif.jpg',
      hover: './assets/images/yousif-hover.jpg',
      name: 'Yousif',
      fact: 'I used to jump out of airplanes!'
    },
    // {
    //   url: './assets/images/team-carousel.jpg',
    //   name: 'Zach',
    // },
    {
      url: './assets/images/raza.jpg',
      hover: './assets/images/raza-hover.jpg',
      name: 'Raza',
      fact: 'My left foot is a size bigger than my right.'
    },
    {
      url: './assets/images/v.jpg',
      hover: './assets/images/v-hover.jpg',
      name: 'V',
      fact: 'I have 31 first cousins and I know them ALL :)'
    }
  ];

  clientLogos = [{
    url: './assets/images/verizon.png',
    hover: './assets/images/verizon-color.png',
    alt: 'Verizon',
    id: '',
  },
  {
    url: './assets/images/tote.png',
    hover: './assets/images/tote-color.png',
    alt: 'Tote Maritime',
    id: '',
  },
  {
    url: './assets/images/cardinal.png',
    hover: './assets/images/cardinal-color.png',
    alt: 'Cardinal Health',
    id: '',
  },
  {
    url: './assets/images/neustar.png',
    hover: './assets/images/neustar-color.png',
    alt: 'Neustar',
    id: '',
  },
  {
    url: './assets/images/accenture.png',
    hover: './assets/images/accenture-color.png',
    alt: 'Accenture',
    id: '',
  },
  {
    url: './assets/images/capitalone.png',
    hover: './assets/images/capitalone-color.png',
    alt: 'Capital One',
    id: '',
  },
  {
    url: './assets/images/plg.png',
    hover: './assets/images/plg-color.png',
    alt: 'Port Logistics Group',
    id: '',
  },
  {
    url: './assets/images/verisign.png',
    hover: './assets/images/verisign-color.png',
    alt: 'Verisign',
    id: 'verisign'
  },
  {
    url: './assets/images/rackspace.png',
    hover: './assets/images/rackspace-color.png',
    alt: 'Rackspace',
    id: '',
  },
  {
    url: './assets/images/blockone.jpeg',
    hover: './assets/images/blockone-color.png',
    alt: 'Block One Capital',
    id: '',
  }
  ]

  itemWidth = 0;
  count = 0;
  slideX = 0;
  @Input() timing = '30s';
  @Input() showControls = true;
  private currentSlide = 0;


  constructor() { }

  ngOnInit() {
  }

  scrollToElement($element): void {
    $element.scrollIntoView({ behavior: "smooth", block: "start", inline: "nearest" });
  }

  scrollCarousel(event: MouseEvent, dir: number) {

    event.stopPropagation();
    const viewWidth = window.innerWidth;
    const div = (this.roll.nativeElement as HTMLDivElement);
    const max = (this.roll.nativeElement as HTMLElement).clientWidth - viewWidth;
    const child = (div.querySelector(':scope > div') as HTMLElement);
    const margin = parseInt(window.getComputedStyle(child).marginLeft, 10);
    const childWidth = child.clientWidth + margin * 2;
    const stripWidth = viewWidth - (viewWidth % childWidth);

    if (this.slideX === max) {
      this.slideX = 0;
    } else {
      this.slideX = Math.max(0, Math.min(this.slideX + stripWidth * dir, max));
    }
    div.style.transform = 'translateX(-' + this.slideX + 'px)';
    return false;
  }


}


