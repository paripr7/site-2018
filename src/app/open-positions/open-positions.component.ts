import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-open-positions',
  templateUrl: './open-positions.component.html',
  styleUrls: ['./open-positions.component.scss']
})
export class OpenPositionsComponent implements OnInit {
  show = false;
  positions = [{
      name: "APPLICATION INNOVATOR",
      desc: "Utilize computer science fundamentals and expertise in at least one programming language to produce enterprise web applications in small teams"
    },
    {
      name: ".NET INNOVATOR",
      desc: "Utilize the ASP.Net framework and C# to produce enterprise web applications with our .Net team"
    },
    {
      name: "UI/UX INNOVATOR",
      desc: "Design and develop the layout and function of enterprise web applications"
    }
  ]

  constructor() { }

  ngOnInit() {
  }

}
